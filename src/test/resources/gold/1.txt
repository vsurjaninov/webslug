 A great summer trilby hat in ivory for both, men and women, the flexible toyo straw
 fedora is trimmed with an attractive striped hatband and side feather. The style
 is distinguished by its pinch-front crown and nice short brim. Hat Detail:- 4"
 pinch front crown.- 1 1/2" snap brim.- Made of: 100% toyo straw.- Elasticized terrycloth
 sweatband.- Removeable side feather.- Striped band. Sizing Info:Small - 55cm, Medium
 - 57cm, Large - 59cm, X-Large - 61cm, XX-Large - 63cm. Sizing Info:Small - 55cm,
 Medium - 57cm, Large - 59cm, X-Large - 61cm, XX-Large - 63cm. 