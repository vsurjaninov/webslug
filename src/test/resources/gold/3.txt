 For a while, it seemed as though public sentiment in media and the news was shifting
 toward acceptance and a change in the way we perceive people...fashion modeling
 became open to those who more closely resembled "real" people; the media was getting
 pressure to make changes in the types of people they portray in advertisements
 and in print to better reflect reality as well, the feeling of hope that settled
 into the states at the election of their first black president seemed to fill the
 world with hope. But just as the American President's approval rating has started
 to decline, largely over matters only Congress can affect, so has the level of
 tolerance, and it's not just America that feels the effect, but everyone who has
 access to American TV programming. How does this affect teenagers? To begin with,
 when our tolerance levels are low as a community or a nation, we start judging
 people by their appearances, and it makes it much more difficult to raise tolerant
 teens. If you look like you come from the Middle East, you must be Arabic, a member
 of a jihad, and about to blow up someone or something. If you are fat, you must
 be lazy, unintelligent, and spend most of your time thinking about food. If you
 look Latin American and live in the Southwestern U.S. or California, you must be
 an illegal alien. Are these harsh-sounding comments? Of course. And they're not
 true at all, no more true than any other stereotype. Unfortunately, when the media,
 attempt to keep sponsors happy by keeping ratings up by featuring deplorable and
 often out-of-context news bytes, people succumb to the intolerance around them
 and begin to judge everyone more harshly. Your teen may not only be exposed to
 bigoted reactions but may begin to exhibit forms of bigotry. How do you raise tolerant
 teens in an intolerant world? It's not easy. We all have prejudices and biases
 to overcome. But if you really want to raise a tolerant teen, a teen who can be
 part of a generation that ushers in peaceful coexistence in our world, the message
 has to come from you, from home. Tolerant teens are teens who are told, by their
 parents first, that it's OK to be different, that it's OK to be accepting of people
 who are different, that difference is what gives communities and neighborhoods
 strength. It goes beyond words, however; the way to really raise tolerant teens
 is to set an example of tolerance. For many, this means occasionally having to
 bite your tongue rather than comment. In some cases, it may mean explaining to
 your teen that you, too, are learning to be tolerant and retract what you've said.
 If you want a world that doesn't judge people by the way they look, the people
 they associate with, the friends they have, the church they go to (or don't), and
 what they do; if you want a world your teen will be able to raise your grandchildren
 in safely, the message of tolerance has to start with you. 