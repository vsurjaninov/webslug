package de.jflex;


import java.io.IOException;
import ru.vsurjaninov.webslug.model.TokenType;
import static ru.vsurjaninov.webslug.model.TokenType.*;


%%

%class TextScanner
%public
%unicode
%ignorecase
%integer
%function getNextTokenInternal
%pack
%char

%{

private int afterCommentState;

public String getText() {
    return new String(zzBuffer, zzStartRead, zzMarkedPos-zzStartRead);
}

public String getText(final int start, final int end) {
    return new String(zzBuffer, zzStartRead, zzMarkedPos-zzStartRead);
}

public int getCurPosistion() {
    return yychar;
}

public TokenType getNextToken() {
    try {
        final int id = getNextTokenInternal();
        if (id == -1) {
            return TokenType.EOF;
        }
        return TokenType.values()[id];
    } catch (IOException ex) {
        throw new RuntimeException(ex);
    }
}

%}


HOST_WORD  = ({DIGIT}|{LETTER})+
EMAIL      =  {HOST_WORD} (("."|"-"|"_") {HOST_WORD})* "@" {HOST_WORD} (("."|"-") {HOST_WORD})+

HOST       =  {HOST_WORD} ((".") {HOST_WORD})*"."{PRIM_DOMAIN}

APOSTROPHE =  {ALPHA} ("'" {ALPHA})+

ACRONYM    =  {LETTER} "." ({LETTER} ".")+

HYPHEN     =  {ALPHA}"-"({ALPHA}|({NUM})+)("-"({ALPHA}|{NUM})+)*

COMPANY    =  {ALPHA} ("&"|"@") {ALPHA}

// url
IP_DIGIT = [0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5]
PORT = [1-9][0-9]{0,3}
IP = ({IP_DIGIT}.){3}{IP_DIGIT}
ESCAPE = %[0-9A-F][0-9A-F]
NAME = ([^\n\r%/<>:.\\#\" ]|{ESCAPE})+
PRIM_DOMAIN = {LETTER}{2,3}
SCHEMA = "http"|"ftp"|"https"|"nntp"|"file"
URL = {SCHEMA}"://"({HOST}|{IP})(":"{PORT})?("/"{NAME})*("/"|("/"{NAME}"."{NAME}("#"{NAME})?))?


NUM = ['+''-']?{DIGIT}(['.'',']?{DIGIT})*

PUNCTUATION	 = ("_"|"-"|"/"|"."|","|";"|":"|"?"|"!"|"("|")"|"'")

ALPHANUM_MIX   = ({DIGIT}+{LETTER}({LETTER}|{DIGIT})*) | ({LETTER}+{DIGIT}({LETTER}|{DIGIT})*)

LETTER     = [\u0041-\u005a\u0061-\u007a\u00c0-\u00d6\u00d8-\u00f6\u00f8-\u00ff\u0100-\u1fff\uffa0-\uffdc]

DIGIT      = [\u0030-\u0039\u0660-\u0669\u06f0-\u06f9\u0966-\u096f\u09e6-\u09ef\u0a66-\u0a6f\u0ae6-\u0aef\u0b66-\u0b6f\u0be7-\u0bef\u0c66-\u0c6f\u0ce6-\u0cef\u0d66-\u0d6f\u0e50-\u0e59\u0ed0-\u0ed9\u1040-\u1049]

ALPHA      = ({LETTER})+

// unprocessed tag
UTAG = "<" "\\"? {LETTER}+ (" "{LETTER}+"=" "\"" ({LETTER} | {NUM} | {DIGIT})* "\"")* ">"

EMPTY_LINE = \n\n | \r\n\r\n
WHITESPACE = \r\n | [ \r\n\t\f]

%%

<YYINITIAL> {
  {ALPHA}        { return WORD.ordinal(); }
  {ALPHANUM_MIX} { return MIX.ordinal(); }
  {APOSTROPHE}   { return WORD.ordinal(); }
  {HYPHEN}       { return WORD.ordinal(); }
  {ACRONYM}      { return ACRONYM.ordinal(); }
  {COMPANY}      { return MIX.ordinal(); }
  {EMAIL}        { return EMAIL.ordinal(); }
  {NUM}          { return NUMBER.ordinal(); }
  {HOST}         { return HOST.ordinal(); }
  {URL}          { return URL.ordinal(); }
  {PUNCTUATION}  { return PUNCTUATION.ordinal(); }
  {EMPTY_LINE}   { return EMPTY_LINE.ordinal(); }
  {UTAG}         { return UNPROCESSED_TAG.ordinal(); }
  {WHITESPACE}   {  }
  . { return SYMBOL.ordinal();  }
}
