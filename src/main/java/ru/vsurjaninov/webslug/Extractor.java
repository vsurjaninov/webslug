package ru.vsurjaninov.webslug;

import ru.vsurjaninov.webslug.clf.segment.BestSegmentFeatures;
import ru.vsurjaninov.webslug.clf.segment.ISegmentClassifier;
import ru.vsurjaninov.webslug.processing.Content;
import ru.vsurjaninov.webslug.processing.Processor;
import ru.vsurjaninov.webslug.processing.SegmentTrainer;
import ru.vsurjaninov.webslug.renderer.IRenderer;
import ru.vsurjaninov.webslug.renderer.WithoutJSRenderer;
import ru.vsurjaninov.webslug.util.Score;
import ru.vsurjaninov.webslug.util.StringUtil;
import ru.vsurjaninov.webslug.util.Tokenizer;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public final class Extractor {

    final private SegmentTrainer trainer;
    final private Processor processor;

    public Extractor(IRenderer renderer, ISegmentClassifier segmentClassifier){
        trainer = new SegmentTrainer(renderer);
        processor = new Processor(renderer, segmentClassifier);
    }

    public static Extractor getDefaultInstance(){
        return new Extractor(new WithoutJSRenderer(), new BestSegmentFeatures());
    }

    public void writeTrainSet(Path htmlDir, Path goldDir, Path outDir){

        final String TRAIN_SET_NAME ="trainset.csv";
        final String LABEL_NAME ="LABEL";
        final String SEPARATOR = ",";

        if(!Files.exists(outDir)){
            try {
                Files.createDirectory(outDir);
            } catch (IOException e) {
                System.err.println("Failed to create output directory");
                System.err.println(e.toString());
            }
        }

        final Path out = Paths.get(outDir + "/" + TRAIN_SET_NAME);

        try(final DirectoryStream<Path> htmlFiles = Files.newDirectoryStream(htmlDir);
            final DirectoryStream<Path> goldFiles = Files.newDirectoryStream(goldDir);
            final PrintWriter trainWriter = new PrintWriter(Files.newOutputStream(out))
        ) {
            trainWriter.print(LABEL_NAME);
            for(String featureName: trainer.getFeaturesNames()){
                trainWriter.print(SEPARATOR);
                trainWriter.print(featureName);
            }
            trainWriter.println();

            final Iterator<Path> htmlIterator = htmlFiles.iterator();
            final Iterator<Path> goldIterator = goldFiles.iterator();

            while(htmlIterator.hasNext() && goldIterator.hasNext()){
                final String gold = StringUtil.readString(goldIterator.next());
                trainer.processPage(htmlIterator.next());

                final Set<String> goldTokens = Tokenizer.getTextSet(gold);
                for(final SegmentTrainer.TrainData data: trainer.getTrainData()){
                    trainWriter.print(data.getSegmentType(goldTokens));
                    for(final Number value: data.getValues()){
                        trainWriter.print(SEPARATOR);
                        trainWriter.print(value);
                    }
                    trainWriter.println();
                }
            }
        } catch (IOException e) {
            System.err.println("Failed to write training set");
            System.err.println(e.toString());
        }
    }

    public Content extractContent(String url) throws IOException {
            processor.processPageFromUrl(url);
            return processor.getContent();
    }

    public Content extractContent(Path path) throws IOException {
        processor.processPageFromPath(path);
        return processor.getContent();
    }

    public String cleanText(String url) throws IOException{
            processor.processPageFromUrl(url);
            return processor.getContent().toString();
    }

    public  String cleanText(Path path) throws IOException{
            processor.processPageFromPath(path);
            return processor.getContent().toString();
    }

    public void writeCleanText(Path htmlDir, Path outDir){
        try(final DirectoryStream<Path> htmlFiles = Files.newDirectoryStream(htmlDir)){
            if(!Files.exists(outDir)){
                Files.createDirectory(outDir);
            }

            for(final Path htmlFile: htmlFiles){
                processor.processPageFromPath(htmlFile);
                final String text = processor.getCleanText();
                final String name = outDir + "/" + htmlFile.getFileName().toString().replaceFirst(".html", ".txt");
                Files.write(Paths.get(name), text.getBytes());
            }

        } catch (IOException e) {
            System.err.println("Failed to extract text from dataset");
            System.err.println(e.toString());
        }
    }

    public static void compareDataSets(Path testDir, Path goldDir){

        try(final DirectoryStream<Path> testFiles = Files.newDirectoryStream(testDir);
            final DirectoryStream<Path> goldFiles = Files.newDirectoryStream(goldDir)
        ){
            final Iterator<Path> testFilesIterator = testFiles.iterator();
            final Iterator<Path> goldFilesIterator = goldFiles.iterator();

            final List<Score> scores = new ArrayList<>();

            while (testFilesIterator.hasNext() && goldFilesIterator.hasNext()){
                final String testText = StringUtil.readString(testFilesIterator.next());
                final String goldText = StringUtil.readString(goldFilesIterator.next());
                scores.add(Score.compareTexts(testText, goldText));
            }

            final Score mean = Score.mean(scores);
            final Score std = Score.std(scores, mean);

            System.out.println("Precision\t" + mean.getPrecision() + " +/- " + std.getPrecision());
            System.out.println("Recall\t" + mean.getRecall() + " +/- " + std.getRecall());
            System.out.println("F1 Score\t" + mean.getF1() + " +/- " + std.getF1());

        } catch (IOException e) {
            System.err.println("Failed to compare datasets");
            System.err.println(e.toString());
        }
    }
}
