package ru.vsurjaninov.webslug.feature.impl;

import ru.vsurjaninov.webslug.feature.IFeature;
import ru.vsurjaninov.webslug.model.Chunk;

final public class CountChildren implements IFeature {
    public Number apply(Chunk chunk) {
        return chunk.getElementProvider().childNodeSize();
    }
}
