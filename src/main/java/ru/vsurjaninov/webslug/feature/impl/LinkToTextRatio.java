package ru.vsurjaninov.webslug.feature.impl;

import ru.vsurjaninov.webslug.feature.IFeature;
import ru.vsurjaninov.webslug.model.Chunk;
import ru.vsurjaninov.webslug.model.Token;
import ru.vsurjaninov.webslug.model.TokenType;
import ru.vsurjaninov.webslug.model.providers.NodeProvider;

import static ru.vsurjaninov.webslug.util.MathUtil.div;

final public class LinkToTextRatio implements IFeature {

    @Override
    public Number apply(Chunk chunk) {
        int numWords = 0;
        for(final Token token: chunk.getTokens()){
            final TokenType type = token.getType();
            if((type != TokenType.SYMBOL)
                    &&(type != TokenType.PUNCTUATION)
                    &&(type != TokenType.EMPTY_LINE)) {
                numWords++;
            }
        }
        int numLinks = 0;

        for(final NodeProvider node: chunk.getElementProvider().childNodes()){
            final String nodeName = node.nodeName();
            if(nodeName.equals("a") || nodeName.equals("link")){
                numLinks++;
            }
        }

        return div(numLinks, numWords);
    }
}
