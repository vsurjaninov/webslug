package ru.vsurjaninov.webslug.feature.impl;

import ru.vsurjaninov.webslug.feature.IFeature;
import ru.vsurjaninov.webslug.model.Chunk;
import ru.vsurjaninov.webslug.model.providers.ElementProvider;

final public class CountDiv implements IFeature {
    @Override
    public Number apply(Chunk chunk) {
        int sum = 0;
        for (final ElementProvider child : chunk.getElementProvider().children())
            if (child.nodeName().equals("div")) {
                sum++;
            }
        return sum;
    }
}
