package ru.vsurjaninov.webslug.feature.impl;

import ru.vsurjaninov.webslug.feature.IFeature;
import ru.vsurjaninov.webslug.model.Chunk;

final public class PrevFeature implements IFeature {

    final private IFeature feature;
    public PrevFeature(IFeature feature){
        this.feature = feature;
    }

    @Override
    public Number apply(Chunk chunk) {
        if(chunk.hasPrevChunk()){
            //TODO: fix
            //it's not optimal approach, but it works without NPE
            return feature.apply(chunk.getPrevChunk());
        } else {
            return 0;
        }
    }
}
