package ru.vsurjaninov.webslug.feature.impl;

import ru.vsurjaninov.webslug.feature.IFeature;
import ru.vsurjaninov.webslug.model.Chunk;
import ru.vsurjaninov.webslug.model.providers.ElementProvider;

final public class NodeHeight implements IFeature {

    @Override
    public Number apply(Chunk chunk) {
        ElementProvider element = chunk.getElementProvider();
        int width = 0;
        while ((element = element.parent()) != null){
            width++;
        }
        return width;
    }
}
