package ru.vsurjaninov.webslug.feature.impl;

import ru.vsurjaninov.webslug.feature.IFeature;
import ru.vsurjaninov.webslug.model.Chunk;

final public class NextFeature implements IFeature {

    final private IFeature feature;
    public NextFeature(IFeature feature){
        this.feature = feature;
    }

    @Override
    public Number apply(Chunk chunk) {
        if(chunk.hasNextChunk()){
            //TODO: fix
            //it's not optimal approach, but it works without NPE
            return feature.apply(chunk.getNextChunk());
        } else {
            return 0;
        }
    }
}
