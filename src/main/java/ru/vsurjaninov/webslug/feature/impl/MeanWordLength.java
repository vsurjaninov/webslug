package ru.vsurjaninov.webslug.feature.impl;

import ru.vsurjaninov.webslug.feature.IFeature;
import ru.vsurjaninov.webslug.model.Chunk;
import ru.vsurjaninov.webslug.model.Token;
import ru.vsurjaninov.webslug.model.TokenType;

import java.util.List;

import static ru.vsurjaninov.webslug.util.MathUtil.div;

final public class MeanWordLength implements IFeature {
    @Override
    public Number apply(Chunk chunk) {
        final List<Token> tokens = chunk.getTokens();
        int sum = 0;

        if (tokens.size() != 0) {
            for (final Token token : tokens) {
                if(token.getType() == TokenType.WORD) {
                    sum += token.getText().length();
                }
            }
            return div(sum, tokens.size());
        } else {
            return 0;
        }
    }
}
