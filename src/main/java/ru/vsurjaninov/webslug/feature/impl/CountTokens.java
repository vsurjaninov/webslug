package ru.vsurjaninov.webslug.feature.impl;

import ru.vsurjaninov.webslug.feature.IFeature;
import ru.vsurjaninov.webslug.model.Chunk;

final public class CountTokens implements IFeature {

    @Override
    public Number apply(Chunk chunk) {
        return chunk.getTokens().size();
    }
}
