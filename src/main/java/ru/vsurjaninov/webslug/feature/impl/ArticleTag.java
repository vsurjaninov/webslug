package ru.vsurjaninov.webslug.feature.impl;

import ru.vsurjaninov.webslug.feature.IFeature;
import ru.vsurjaninov.webslug.model.Chunk;
import ru.vsurjaninov.webslug.model.providers.ElementProvider;

final public class ArticleTag implements IFeature {

    @Override
    public Number apply(Chunk chunk) {
        final ElementProvider chunkElement = chunk.getElementProvider();
        for(final String tag: tags){
            if(chunkElement.nodeName().equals(tag)){
                return 1;
            }
        }
        return 0;
    }

    private final static String[] tags = new String[]{
            "p",
            "h",
            "h1",
            "h2",
            "h3",
            "h4",
            "h5",
            "i",
            "b",
    };
}
