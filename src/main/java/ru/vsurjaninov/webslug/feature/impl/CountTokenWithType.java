package ru.vsurjaninov.webslug.feature.impl;

import ru.vsurjaninov.webslug.feature.IFeature;
import ru.vsurjaninov.webslug.model.Chunk;
import ru.vsurjaninov.webslug.model.Token;
import ru.vsurjaninov.webslug.model.TokenType;

public final class CountTokenWithType implements IFeature {

    private TokenType type;

    public CountTokenWithType(TokenType type){
        this.type = type;
    }

    @Override
    public Number apply(Chunk chunk) {
        int sum = 0;
        for(final Token token:chunk.getTokens()){
            if(token.getType() == type){
                sum++;
            }
        }
        return sum;
    }
}
