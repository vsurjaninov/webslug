package ru.vsurjaninov.webslug.feature.impl;

import ru.vsurjaninov.webslug.feature.IFeature;
import ru.vsurjaninov.webslug.model.Chunk;
import ru.vsurjaninov.webslug.model.providers.ElementProvider;

final public class HeadersAround implements IFeature {
    @Override
    public Number apply(Chunk chunk) {
        int sum = 0;
        for(final ElementProvider child: chunk.getElementProvider().children())
            for(final String tag: tags){
                if(child.nodeName().equals(tag)){
                    sum++;
                }
            }
        return sum;
    }

    private final static String[] tags = new String[]{
            "h1",
            "h2",
            "h3",
            "h4",
            "h5",
    };
}
