package ru.vsurjaninov.webslug.feature;

import ru.vsurjaninov.webslug.model.Chunk;

public interface IFeature {
    public Number apply(Chunk chunk);
}
