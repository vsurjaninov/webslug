package ru.vsurjaninov.webslug.feature;

import ru.vsurjaninov.webslug.feature.impl.*;
import ru.vsurjaninov.webslug.model.Chunk;
import ru.vsurjaninov.webslug.model.TokenType;

public enum Feature implements IFeature{

    ARTICLE_TAG(new ArticleTag()),
    CHILDREN_COUNT(new CountChildren()),
    DIV_COUNT(new CountDiv()),
    NEIGHBORS_COUNT(new CountNeighbors()),
    TOKENS_COUNT(new CountTokens()),
    HEADERS_AROUND(new HeadersAround()),
    INNER_HTML_LENGTH(new InnerHtmlLength()),
    LINK_TO_TEXT_RATIO(new LinkToTextRatio()),
    MEAN_WORD_LENGTH(new MeanWordLength()),
    NODE_HEIGHT(new NodeHeight()),
    WORDS_COUNT(new CountTokenWithType(TokenType.WORD)),
    NUMBERS_COUNT(new CountTokenWithType(TokenType.NUMBER)),
    PUNCTUATION_COUNT(new CountTokenWithType(TokenType.PUNCTUATION)),

    NEXT_ARTICLE_TAG(new NextFeature(ARTICLE_TAG)),
    NEXT_CHILDREN_COUNT(new NextFeature(CHILDREN_COUNT)),
    NEXT_NEIGHBORS_COUNT(new NextFeature(NEIGHBORS_COUNT)),
    NEXT_TOKENS_COUNT(new NextFeature(TOKENS_COUNT)),
    NEXT_HEADERS_AROUND(new NextFeature(HEADERS_AROUND)),
    NEXT_INNER_HTML_LENGTH(new NextFeature(INNER_HTML_LENGTH)),
    NEXT_LINK_TO_TEXT_RATIO(new NextFeature(LINK_TO_TEXT_RATIO)),
    NEXT_MEAN_WORD_LENGTH(new NextFeature(MEAN_WORD_LENGTH)),
    NEXT_NODE_HEIGHT(new NextFeature(NODE_HEIGHT)),
    NEXT_WORDS_COUNT(new NextFeature(WORDS_COUNT)),
    NEXT_NUMBERS_COUNT(new NextFeature(NUMBERS_COUNT)),
    NEXT_PUNCTUATION_COUNT(new NextFeature(PUNCTUATION_COUNT)),

    PREV_ARTICLE_TAG(new PrevFeature(ARTICLE_TAG)),
    PREV_CHILDREN_COUNT(new PrevFeature(CHILDREN_COUNT)),
    PREV_DIV_COUNT(new PrevFeature(DIV_COUNT)),
    PREV_NEIGHBORS_COUNT(new PrevFeature(NEIGHBORS_COUNT)),
    PREV_TOKENS_COUNT(new PrevFeature(TOKENS_COUNT)),
    PREV_HEADERS_AROUND(new PrevFeature(HEADERS_AROUND)),
    PREV_INNER_HTML_LENGTH(new PrevFeature(INNER_HTML_LENGTH)),
    PREV_LINK_TO_TEXT_RATIO(new PrevFeature(LINK_TO_TEXT_RATIO)),
    PREV_MEAN_WORD_LENGTH(new PrevFeature(MEAN_WORD_LENGTH)),
    PREV_NODE_HEIGHT(new PrevFeature(NODE_HEIGHT)),
    PREV_WORDS_COUNT(new PrevFeature(WORDS_COUNT)),
    PREV_NUMBERS_COUNT(new PrevFeature(NUMBERS_COUNT)),
    PREV_PUNCTUATION_COUNT(new PrevFeature(PUNCTUATION_COUNT)),
//    NEXT_(new NextFeature()),
//    PREV_(new PrevFeature()),
    ;

    private IFeature feature;
    Feature(IFeature feature) {
        this.feature = feature;
    }

    @Override
    public Number apply(Chunk chunk){
        return feature.apply(chunk);
    }
}
