package ru.vsurjaninov.webslug.model;

public final class Token {
    private final String text;
    private final TokenType type;

    public Token(TokenType type, String text){
        this.type = type;
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public TokenType getType() {
        return type;
    }
}

