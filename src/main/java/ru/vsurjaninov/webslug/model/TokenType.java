package ru.vsurjaninov.webslug.model;

public enum TokenType {
    WORD, NUMBER, PUNCTUATION, SYMBOL, MIX, ACRONYM, EMAIL, URL, HOST, EMPTY_LINE, UNPROCESSED_TAG, EOF
}
