package ru.vsurjaninov.webslug.model;

import ru.vsurjaninov.webslug.clf.segment.SegmentType;
import ru.vsurjaninov.webslug.feature.Feature;
import ru.vsurjaninov.webslug.feature.IFeature;
import ru.vsurjaninov.webslug.model.providers.ElementProvider;
import ru.vsurjaninov.webslug.model.providers.NodeProvider;
import ru.vsurjaninov.webslug.util.Tokenizer;

import java.util.HashMap;
import java.util.List;


public final class Chunk {
    private SegmentType type;
    private final List<Token> tokens;
    final private NodeProvider node;
    final private  HashMap<IFeature, Number> featuresMap = new HashMap<>();
    private Chunk prevChunk;
    private Chunk nextChunk;

    public Chunk(String text, NodeProvider node){
        this.tokens = Tokenizer.createTokens(text);
        this.node = node;
    }

    public List<Token> getTokens(){
        return tokens;
    }

    public String toString(){
        return node.text();
    }

    public ElementProvider getElementProvider() {
        return node.getElementProvider();
    }

    public NodeProvider getNode(){
        return node;
    }

    public void addFeature(IFeature feature, Number value){
        featuresMap.put(feature, value);
    }

    public int getIntFeature(IFeature feature){
        return getFeature(feature).intValue();
    }

    public double getDoubleFeature(IFeature feature){
        return getFeature(feature).doubleValue();
    }

    public Number getFeature(IFeature feature){
        return featuresMap.get(feature);
    }

    public Number[] getFeaturesValues(Feature[] features){
        Number[] values = new Number[features.length];
        int i = 0;
        for(IFeature feature: features){
            values[i] = this.featuresMap.get(feature);
            i++;
        }

        return values;
    }

    public double[] getDoubleFeaturesValues(IFeature[] features){
        final double[] values = new double[features.length];
        for(int i = 0; i < features.length; i++){
            values[i] = this.featuresMap.get(features[i]).doubleValue();
        }

        return values;
    }

    public int[] getIntFeaturesValues(IFeature[] features){
        final int[] values = new int[features.length];
        for(int i = 0; i < features.length; i++){
            values[i] = this.featuresMap.get(features[i]).intValue();
        }
        return values;
    }

    public void setSegmentType(SegmentType type) {
        this.type = type;
    }

    public SegmentType getType() {
        return this.type;
    }

    public void setPrevChunk(Chunk prevChunk) {
        this.prevChunk = prevChunk;
    }

    public void setNextChunk(Chunk nextChunk) {
        this.nextChunk = nextChunk;
    }

    public boolean hasPrevChunk(){
        return prevChunk != null;
    }

    public boolean hasNextChunk(){
        return nextChunk != null;
    }

    public Chunk getPrevChunk() {
        return prevChunk;
    }

    public Chunk getNextChunk() {
        return nextChunk;
    }
}
