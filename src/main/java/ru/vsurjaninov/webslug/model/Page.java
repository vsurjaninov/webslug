package ru.vsurjaninov.webslug.model;

import ru.vsurjaninov.webslug.clf.segment.ISegmentClassifier;
import ru.vsurjaninov.webslug.clf.segment.SegmentType;
import ru.vsurjaninov.webslug.feature.IFeature;
import ru.vsurjaninov.webslug.filter.IFilter;
import ru.vsurjaninov.webslug.model.providers.ChunksListCreator;
import ru.vsurjaninov.webslug.model.providers.ElementProvider;
import ru.vsurjaninov.webslug.model.providers.ElementUtil;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public final class Page {

    private ElementProvider root;
    private String title;
    private List<Chunk> chunks;

    public void extractChunks(String html){
        root = new ElementProvider(html);
        title = ElementUtil.getTitle(root);
        chunks = ChunksListCreator.extract(root);
    }

    public String getTitle() {
        return title;
    }

    public List<Chunk> getChunks(){
        return chunks;
    }

    public void applyFilter(IFilter filter){
        final Iterator<Chunk> it = chunks.listIterator();
        while(it.hasNext()){
            final Chunk chunk = it.next();
            if(filter.apply(chunk)){
                if(chunk.hasPrevChunk()){
                    chunk.getPrevChunk().setNextChunk(chunk.getNextChunk());
                }
                if(chunk.hasNextChunk()){
                    chunk.getNextChunk().setPrevChunk(chunk.getPrevChunk());
                }
                it.remove();
            }
        }
    }

    public void applyFeature(IFeature feature){
        for(final Chunk chunk: chunks){
            chunk.addFeature(feature, feature.apply(chunk));
        }
    }

    public void  applyFeatures(IFeature[] features){
        for(final IFeature feature: features){
            this.applyFeature(feature);
        }
    }

    public void applyClassifier(ISegmentClassifier classifier){
        for(final Chunk chunk: chunks){
            chunk.setSegmentType(classifier.apply(chunk));
        }
    }

    public List<Chunk> getChunksWithType(SegmentType type){
        List<Chunk> chunks = new ArrayList<>();
        for(final Chunk chunk: this.chunks){
            if(chunk.getType().equals(type)){
                chunks.add(chunk);
            }
        }
        return chunks;
    }
}
