package ru.vsurjaninov.webslug.model.providers;

import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.NodeTraversor;
import org.jsoup.select.NodeVisitor;
import ru.vsurjaninov.webslug.model.Chunk;

import java.util.ArrayList;
import java.util.List;

final public class ChunksListCreator {

    public static List<Chunk> extract(ElementProvider root){
        final TextVisitor formatter = new TextVisitor();
        final NodeTraversor traversor = new NodeTraversor(formatter);
        traversor.traverse(root.getElement());
        return formatter.getChunks();
    }

    private static class TextVisitor implements NodeVisitor {

        private final List<Chunk> chunks = new ArrayList<>();
        private Chunk tmpChunk = null;

        public List<Chunk> getChunks() {
            return chunks;
        }

        @Override
        public void head(Node node, int i) {
            if (node instanceof TextNode) {
                final String text = ((TextNode) node).text();
                if (text.equals(" ") || text.equals("\n") || text.equals("\t") || text.equals("\r"))
                    return;

                Chunk chunk = new Chunk(text, new NodeProvider(node));
                chunk.setPrevChunk(tmpChunk);
                if(tmpChunk != null){
                    tmpChunk.setNextChunk(chunk);
                }
                tmpChunk = chunk;
                chunks.add(chunk);
            }
        }

        @Override
        public void tail(Node node, int i) {
        }
    }
}
