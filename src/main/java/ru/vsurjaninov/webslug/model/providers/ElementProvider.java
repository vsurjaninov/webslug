package ru.vsurjaninov.webslug.model.providers;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;

import java.util.ArrayList;
import java.util.List;

public final class ElementProvider {
    private  final Element element;

    public ElementProvider(Element element){
        this.element = element;
    }

    public ElementProvider(Node node){
        this.element = (Element)node;
    }

    public ElementProvider(String processedHtml){
        this.element = Jsoup.parse(processedHtml);
    }

    public String nodeName(){
        return element.nodeName();
    }

    public String text(){
        return element.text();
    }

    public ElementProvider parent(){
        if(element.parent() != null) {
            return new ElementProvider(element.parent());
        } else {
            return null;
        }
    }

    public String outerHtml(){
        return element.outerHtml();
    }

    public List<NodeProvider> childNodes(){
        List<NodeProvider> childNodes = new ArrayList<>();
        for(Node n: element.childNodes()){
            childNodes.add(new NodeProvider(n));
        }
        return childNodes;
    }

    protected Element getElement(){
        return element;
    }

    public List<ElementProvider> children(){
        List<ElementProvider> children = new ArrayList<>();
        for(Element e: element.children()){
            children.add(new ElementProvider(e));
        }
        return children;
    }

    public int childNodeSize(){
        return element.childNodeSize();
    }
}
