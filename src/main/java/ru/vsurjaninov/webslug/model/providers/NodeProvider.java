package ru.vsurjaninov.webslug.model.providers;

import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;

import java.util.ArrayList;
import java.util.List;

public final class NodeProvider {
    private final Node node;

    public NodeProvider(Node node){
        this.node = node;
    }

    public String nodeName(){
        return node.nodeName();
    }

    public String text(){
        if(node instanceof TextNode){
            return ((TextNode) node).text();
        } else {
            return "";
        }
    }

    public NodeProvider parent(){
        return new NodeProvider(node.parent());
    }

    public String outerHtml(){
       return node.outerHtml();
    }

    public List<NodeProvider> childNodes(){
        List<NodeProvider> childNodes = new ArrayList<>();
        for(Node n: node.childNodes()){
            childNodes.add(new NodeProvider(n));
        }
        return childNodes;
    }

    protected Node getNode(){
        return node;
    }

    public ElementProvider getElementProvider(){
        return new ElementProvider(node.parent());
    }
}
