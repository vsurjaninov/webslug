package ru.vsurjaninov.webslug.model.providers;

final public class ElementUtil {
    public static String getTitle(ElementProvider root){
        return root.getElement().getElementsByTag("title").text();
    }
}
