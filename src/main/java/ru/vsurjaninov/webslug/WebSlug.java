package ru.vsurjaninov.webslug;

import joptsimple.OptionParser;
import joptsimple.OptionSet;
import joptsimple.OptionSpec;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public final class WebSlug {

    public static void main(String[] args) {

        final Extractor extractor = Extractor.getDefaultInstance();

        final OptionParser parser = new OptionParser("etvu:f:d::");

        final OptionSpec<String> textDirOpt =
                parser.accepts("textdir").withRequiredArg().ofType(String.class);
        final OptionSpec<String> htmlDirOpt =
                parser.accepts("htmldir").withRequiredArg().ofType(String.class);
        final OptionSpec<String> goldDirOpt =
                parser.accepts("golddir").withRequiredArg().ofType(String.class);
        final OptionSpec<String> outDirOpt =
                parser.accepts("outdir").withRequiredArg().ofType(String.class).defaultsTo("output");

        final OptionSet options = parser.parse(args);

//        parser.accepts("phantomjs");
//        parser.accepts("htmlunit");


        if(options.has("e")){
            if(options.has("u")){
                final String url = (String) options.valueOf("u");
                try {
                    System.out.println(extractor.cleanText(url));
                } catch (IOException e){
                    System.err.println("Failed to extract text from url");
                    System.err.println(e.toString());
                }

            } else if(options.has("f")){
                final Path path = Paths.get((String) options.valueOf("f"));
                try {
                    System.out.println(extractor.cleanText(path));
                } catch (IOException e){
                    System.err.println("Failed to extract text from file");
                    System.err.println(e.toString());
                }

            } else if(options.has("d")){
                final Path htmlDir = Paths.get(htmlDirOpt.value(options));
                final Path outDir = Paths.get(outDirOpt.value(options));
                extractor.writeCleanText(htmlDir, outDir);
            }
        } else if(options.has("t")){
            final Path htmlDir = Paths.get(htmlDirOpt.value(options));
            final Path goldDir = Paths.get(goldDirOpt.value(options));
            final Path out = Paths.get(outDirOpt.value(options));
            extractor.writeTrainSet(htmlDir, goldDir, out);

        } else if(options.has("v")){
            final Path textDir = Paths.get(textDirOpt.value(options));
            final Path goldDir = Paths.get(goldDirOpt.value(options));
            Extractor.compareDataSets(textDir, goldDir);
        }
    }
}
