package ru.vsurjaninov.webslug.processing;


final public class Content {
    private String title;
    private String text;

    public void setText(String text) {
        this.text = text;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public boolean equals(Object obj) {

        if (this == obj){
            return true;
        }
        if (obj == null || getClass() != obj.getClass()){
            return false;
        }

        Content that = (Content) obj;
        return (that.getTitle().equals(title)
                && that.getText().equals(text));
    }

    @Override
    public String toString() {
        return "Title:\n"
                + title + "\n"
                + "Content:\n"
                + text;
    }
}
