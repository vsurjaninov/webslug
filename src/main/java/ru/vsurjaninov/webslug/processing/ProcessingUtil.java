package ru.vsurjaninov.webslug.processing;

import ru.vsurjaninov.webslug.clf.segment.SegmentType;
import ru.vsurjaninov.webslug.model.Chunk;
import ru.vsurjaninov.webslug.model.Page;
import ru.vsurjaninov.webslug.util.Tokenizer;

import java.util.HashSet;
import java.util.List;

public final class ProcessingUtil {

    public static void siblingsScoresBlur(Page page, SegmentType noiseMark, int radius){
        SegmentType anotherType;
        if (noiseMark.ordinal() == 0){
            anotherType = SegmentType.CLEAN;
        } else {
            anotherType = SegmentType.NOISE;
        }

        final List<Chunk> chunks = page.getChunks();
        for(int i = radius; i < chunks.size() - radius; i++){
            if(chunks.get(i).getType().equals(noiseMark)){
                boolean isNoise = true;
                for(int j = 1; j < radius + 1; j++){
                    isNoise &= (chunks.get(i - j).getType().equals(anotherType));
                    isNoise &= (chunks.get(i + j).getType().equals(anotherType));
                }
                if(isNoise){
                    chunks.get(i).setSegmentType(anotherType);
                }
            }
        }
    }

    public static void parentBlur(Page page){
        final List<Chunk> chunks = page.getChunks();
        for(final Chunk chunk: chunks){
            if(chunk.getType().equals(SegmentType.CLEAN)) {
                final HashSet<String> parentTokens
                        = new HashSet<>(Tokenizer.getTextTokens(chunk.getElementProvider().text()));
                Chunk tmp = chunk;
                while (tmp.hasPrevChunk()){
                    if (tmp.getPrevChunk().getType().equals(SegmentType.NOISE)) {
                        final List<String> prevChunkTokens
                                = Tokenizer.getTextTokens(tmp.getPrevChunk().getElementProvider().text());
                        int count = 0;
                        for (String token : prevChunkTokens) {
                            if (parentTokens.contains(token)) {
                                count++;
                            }
                        }
                        if (count == prevChunkTokens.size()) {
                            tmp.getPrevChunk().setSegmentType(SegmentType.CLEAN);
                        }
                    }
                    tmp = tmp.getPrevChunk();
                }

                if (chunk.hasNextChunk()) {
                    if(chunk.getNextChunk().getType().equals(SegmentType.NOISE)) {
                        final List<String> nextChunkTokens
                                = Tokenizer.getTextTokens(chunk.getNextChunk().getElementProvider().text());
                        int count = 0;
                        for (String token : nextChunkTokens) {
                            if (parentTokens.contains(token)) {
                                count++;
                            }
                        }
                        if (count == nextChunkTokens.size()) {
                            chunk.getNextChunk().setSegmentType(SegmentType.CLEAN);
                        }
                    }
                }
            }
        }
    }
}
