package ru.vsurjaninov.webslug.processing;

import ru.vsurjaninov.webslug.clf.segment.ISegmentClassifier;
import ru.vsurjaninov.webslug.clf.segment.SegmentType;
import ru.vsurjaninov.webslug.filter.EmptyText;
import ru.vsurjaninov.webslug.filter.UnprocessedTag;
import ru.vsurjaninov.webslug.filter.UnwantedTag;
import ru.vsurjaninov.webslug.model.Chunk;
import ru.vsurjaninov.webslug.model.Page;
import ru.vsurjaninov.webslug.renderer.IRenderer;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

public final class Processor {
    final private IRenderer renderer;
    final private ISegmentClassifier segmentClassifier;
    private final Page page = new Page();

    public Processor(IRenderer renderer,
                      ISegmentClassifier segmentClassifier)  {

        this.renderer = renderer;
        this.segmentClassifier = segmentClassifier;
    }

    public void processPageFromPath(Path path) throws IOException {
        processPage(renderer.getDocument(path));
    }

    public void processPageFromUrl(String url) throws IOException {
        processPage(renderer.getDocument(url));
    }

    public void processPage(String html) throws IOException {
        page.extractChunks(html);
        page.applyFeatures(segmentClassifier.getFeatures());
        page.applyFilter(EmptyText.INSTANCE);
        page.applyFilter(UnwantedTag.INSTANCE);
        page.applyFilter(UnprocessedTag.INSTANCE);
        page.applyClassifier(segmentClassifier);
        ProcessingUtil.siblingsScoresBlur(page, SegmentType.NOISE, 1);
    }

    public String getCleanText(){
        final List<Chunk> chunks = page.getChunksWithType(SegmentType.CLEAN);
        final StringBuilder stringBuilder = new StringBuilder();
        for (final Chunk chunk: chunks){
            stringBuilder.append(chunk);
            stringBuilder.append('\n');
        }
        return stringBuilder.toString();
    }

    public Content getContent(){
        Content content = new Content();
        content.setTitle(page.getTitle());
        content.setText(getCleanText());
        return content;
    }
}
