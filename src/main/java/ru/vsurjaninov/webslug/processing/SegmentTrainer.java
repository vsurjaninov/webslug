package ru.vsurjaninov.webslug.processing;

import ru.vsurjaninov.webslug.clf.segment.SegmentType;
import ru.vsurjaninov.webslug.feature.Feature;
import ru.vsurjaninov.webslug.filter.EmptyText;
import ru.vsurjaninov.webslug.filter.UnprocessedTag;
import ru.vsurjaninov.webslug.filter.UnwantedTag;
import ru.vsurjaninov.webslug.model.Chunk;
import ru.vsurjaninov.webslug.model.Page;
import ru.vsurjaninov.webslug.renderer.IRenderer;
import ru.vsurjaninov.webslug.util.Tokenizer;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

final public class SegmentTrainer {
    final private IRenderer renderer;
    final private Page page = new Page();

    public SegmentTrainer(IRenderer renderer)  {
        this.renderer = renderer;
    }

    public void processPage(Path path) throws IOException {
        page.extractChunks(renderer.getDocument(path));
        page.applyFilter(EmptyText.INSTANCE);
        page.applyFilter(UnwantedTag.INSTANCE);
        page.applyFilter(UnprocessedTag.INSTANCE);
        page.applyFeatures(Feature.values());
    }

    public List<String> getFeaturesNames(){
        List<String> names = new ArrayList<>();
        for(Feature feature: Feature.values()){
            names.add(feature.name());
        }
        return names;
    }

    public List<TrainData> getTrainData(){
        List<TrainData> trainDataList = new ArrayList<>();
        for(Chunk chunk: page.getChunks()){
            trainDataList.add(new TrainData(chunk));
        }
        return trainDataList;
    }

    public class TrainData{
        final Set<String> tokens;
        final Number[] values;

        public TrainData(Chunk chunk){
            tokens = new HashSet<>(Tokenizer.getTextTokens(chunk.toString()));
            values = chunk.getFeaturesValues(Feature.values());
        }

        public SegmentType getSegmentType(Set<String> base){

            for(String token: tokens){
                if(!base.contains(token)){
                    return SegmentType.NOISE;
                }
            }

            return SegmentType.CLEAN;
        }

        public Number[] getValues() {
            return values;
        }
    }
}
