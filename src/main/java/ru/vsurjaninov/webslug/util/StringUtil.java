package ru.vsurjaninov.webslug.util;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

public final class StringUtil {

    public static String readString(Path path) throws IOException{
        return new String(Files.readAllBytes(path), StandardCharsets.UTF_8);
    }
}
