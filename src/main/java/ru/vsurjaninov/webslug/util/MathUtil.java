package ru.vsurjaninov.webslug.util;

final public class MathUtil {

    public static double div(double a, double b){

        if(b == 0){
            if(a == 0){
                return 1.0;
            } else {
                return a;
            }
        }

        return a / b;
    }
}
