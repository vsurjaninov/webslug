package ru.vsurjaninov.webslug.util;

import de.jflex.TextScanner;
import ru.vsurjaninov.webslug.model.Token;
import ru.vsurjaninov.webslug.model.TokenType;

import java.io.StringReader;
import java.util.*;


public final class Tokenizer {

    public static boolean isEmptyString(String text){
            return getTextTokens(text).size() == 0;
    }

    public static List<String> getTextTokens(String text){

        final TextScanner scanner = new TextScanner(new StringReader(text));
        final List<String> tokens = new ArrayList<>();
        TokenType token;
        while((token = scanner.getNextToken()) != TokenType.EOF) {
            if((token != TokenType.SYMBOL) &&
               (token != TokenType.PUNCTUATION) &&
               (token != TokenType.UNPROCESSED_TAG) &&
               (token != TokenType.URL) &&
               (token != TokenType.EMPTY_LINE)) {
                tokens.add(scanner.getText());
            }
        }
        return tokens;
    }

    public static List<String> getWordTokens(String text){

        final TextScanner scanner = new TextScanner(new StringReader(text));
        final List<String> tokens = new ArrayList<String>();
        TokenType token;
        while((token = scanner.getNextToken()) != TokenType.EOF) {
            if((token != TokenType.SYMBOL) &&
                    (token != TokenType.PUNCTUATION) &&
                    (token != TokenType.UNPROCESSED_TAG) &&
                    (token != TokenType.URL) &&
                    (token != TokenType.NUMBER) &&
                    (token != TokenType.EMPTY_LINE)) {
                tokens.add(scanner.getText().toLowerCase());
            }
        }
        return tokens;
    }

    public static List<String> tokenize(String text){

        final TextScanner scanner = new TextScanner(new StringReader(text));
        final List<String> tokens = new ArrayList<>();
        while(scanner.getNextToken() != TokenType.EOF) {
            tokens.add(scanner.getText());
        }
        return tokens;
    }

    public static List<Token> createTokens(String text){

        final TextScanner scanner = new TextScanner(new StringReader(text));
        final List<Token> tokens = new LinkedList<>();
        TokenType type;
        while((type = scanner.getNextToken()) != TokenType.EOF) {
            tokens.add(new Token(type, scanner.getText()));
        }
        return tokens;
    }

    public static Set<String> getWordSet(String text){
        return new HashSet<>(getWordTokens(text));
    }

    public static Set<String> getTextSet(String text){
        return new HashSet<>(getTextTokens(text));
    }
}
