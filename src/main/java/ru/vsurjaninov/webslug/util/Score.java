package ru.vsurjaninov.webslug.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static ru.vsurjaninov.webslug.util.MathUtil.div;

final public class Score {

    double precision;
    double recall;

    public Score(){}

    public Score(double precision, double recall){
        this.precision = precision;
        this.recall = recall;
    }

    public static Score compareTexts(String predictedText, String goldText){
        final Set<String> predictedTokens = Tokenizer.getTextSet(predictedText);
        final Set<String> goldTokens = Tokenizer.getTextSet(goldText);

        int tp, fp, fn;
        tp = fp = fn = 0;

        for(final String token: predictedTokens){
            if(goldTokens.contains(token)){
                tp++;
            } else {
                fp++;
            }
        }

        for (final String token: goldTokens){
            if(!predictedTokens.contains(token)){
                fn++;
            }
        }

        return new Score(div(tp, tp + fp), div(tp, tp + fn));
    }

    public static Score mean(List<Score> values){
        double sumPrecision = 0;
        double sumRecall = 0;
        for(final Score value: values){
            sumPrecision += value.getPrecision();
            sumRecall += value.getRecall();
        }

        return new Score(div(sumPrecision, values.size()),
                div(sumRecall, values.size()));
    }

    public static Score std(List<Score> scores, Score meanScore){
        List<Score> diffQuads = new ArrayList<>();
        for(Score score: scores){
            Score dScore = new Score();
            dScore.setPrecision(Math.pow(score.getPrecision() - meanScore.getPrecision(), 2));
            dScore.setRecall(Math.pow(score.getRecall() - meanScore.getRecall(), 2));
            diffQuads.add(dScore);
        }

        Score meanDiff = mean(diffQuads);
        return new Score(Math.sqrt(meanDiff.getPrecision()),
                Math.sqrt(meanDiff.getRecall()));
    }

    public static Score std(List<Score> scores){
        return std(scores, mean(scores));
    }

    public double getPrecision() {
        return precision;
    }

    public double getRecall() {
        return recall;
    }

    public double getF1() {
        return div(2.0 * precision * recall, (precision + recall));
    }

    public void setPrecision(double precision) {
        this.precision = precision;
    }

    public void setRecall(double recall) {
        this.recall = recall;
    }
}
