package ru.vsurjaninov.webslug.renderer;

import java.io.IOException;
import java.nio.file.Path;

public interface IRenderer {
    String getDocument(Path file) throws IOException;
    String getDocument(String url) throws IOException;
}
