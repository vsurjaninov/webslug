package ru.vsurjaninov.webslug.renderer;

import org.jsoup.Jsoup;

import java.io.IOException;
import java.nio.file.Path;

public final class WithoutJSRenderer implements IRenderer {

    //Don't render JavaScript
    @Override
    public String getDocument(Path file) throws IOException {
        return Jsoup.parse(file.toFile(), "UTF-8").html();
    }

    @Override
    public String getDocument(String url) throws IOException {
        return Jsoup.connect(url).get().html();
    }
}
