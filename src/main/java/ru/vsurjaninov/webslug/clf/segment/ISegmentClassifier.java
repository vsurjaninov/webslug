package ru.vsurjaninov.webslug.clf.segment;

import ru.vsurjaninov.webslug.feature.IFeature;
import ru.vsurjaninov.webslug.model.Chunk;

public interface ISegmentClassifier {

    public abstract SegmentType apply(Chunk chunk);
    public abstract IFeature[] getFeatures();
}
