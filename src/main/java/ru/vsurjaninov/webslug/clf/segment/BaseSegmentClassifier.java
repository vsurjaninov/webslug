package ru.vsurjaninov.webslug.clf.segment;

import ru.vsurjaninov.webslug.feature.IFeature;
import ru.vsurjaninov.webslug.model.Chunk;

final public class BaseSegmentClassifier implements ISegmentClassifier {
    @Override
    public SegmentType apply(Chunk chunk) {
        return SegmentType.CLEAN;
    }

    @Override
    public IFeature[] getFeatures() {
        return new IFeature[0];
    }
}
