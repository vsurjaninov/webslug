package ru.vsurjaninov.webslug.clf.segment;

import ru.vsurjaninov.webslug.feature.IFeature;
import ru.vsurjaninov.webslug.model.Chunk;

import static ru.vsurjaninov.webslug.feature.Feature.*;

final public class BestSegmentFeatures implements ISegmentClassifier {

    @Override
    public SegmentType apply(Chunk chunk) {
        if (chunk.getIntFeature(NEIGHBORS_COUNT) < 17) {
            if (chunk.getIntFeature(NODE_HEIGHT) < 25) {
                if (chunk.getIntFeature(NEXT_TOKENS_COUNT) < 20) {
                    if (chunk.getIntFeature(TOKENS_COUNT) < 26) {
                        return SegmentType.NOISE;
                    } else {
                        return SegmentType.CLEAN;
                    }
                } else {
                    return SegmentType.CLEAN;
                }
            } else {
                if (chunk.getDoubleFeature(LINK_TO_TEXT_RATIO) <= 0.8333) {
                    return SegmentType.CLEAN;
                } else {
                    return SegmentType.NOISE;
                }
            }
        } else {
            if (chunk.getIntFeature(TOKENS_COUNT) < 5) {
                if (chunk.getIntFeature(NODE_HEIGHT) < 15) {
                    if (chunk.getIntFeature(NEIGHBORS_COUNT) < 162) {
                        return SegmentType.NOISE;
                    } else {
                        return SegmentType.CLEAN;
                    }
                } else {
                    return SegmentType.CLEAN;
                }
            } else {
                if (chunk.getIntFeature(NEIGHBORS_COUNT) < 154) {
                    if (chunk.getIntFeature(NEIGHBORS_COUNT) < 146) {
                        return SegmentType.CLEAN;
                    } else {
                        return SegmentType.NOISE;
                    }
                } else {
                    return SegmentType.CLEAN;
                }
            }
        }
    }

    @Override
    public IFeature[] getFeatures(){
        return new IFeature[]{
                NEIGHBORS_COUNT,
                TOKENS_COUNT,
                NODE_HEIGHT,
                LINK_TO_TEXT_RATIO,
                NEXT_TOKENS_COUNT,
        };
    }
}
