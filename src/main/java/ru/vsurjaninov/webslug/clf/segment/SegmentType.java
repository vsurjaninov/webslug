package ru.vsurjaninov.webslug.clf.segment;

public enum SegmentType {
    NOISE,
    CLEAN,
}
