package ru.vsurjaninov.webslug.clf.segment;

import ru.vsurjaninov.webslug.feature.IFeature;
import ru.vsurjaninov.webslug.model.Chunk;

import static ru.vsurjaninov.webslug.feature.Feature.*;


final public class  BoilerpipeImmitator implements ISegmentClassifier {

    final IFeature[] features = new IFeature[]{
            WORDS_COUNT,
            PREV_WORDS_COUNT,
            NEXT_WORDS_COUNT,
            LINK_TO_TEXT_RATIO,
            PREV_LINK_TO_TEXT_RATIO,
            NEXT_LINK_TO_TEXT_RATIO,
    };

    @Override
    public IFeature[] getFeatures() {
        return features;
    }

    @Override
    public SegmentType apply(Chunk chunk) {
        double[] values = chunk.getDoubleFeaturesValues(features);
        if(values[3] <= 0.3333){
            if(values[4] <= 0.5556){
                if(values[0] <= 16){
                    if(values[2] <= 15){
                        if(values[1] <= 4){
                            return SegmentType.NOISE;
                        } else {
                            return SegmentType.CLEAN;
                        }
                    } else {
                        return SegmentType.CLEAN;
                    }
                } else {
                    return SegmentType.CLEAN;
                }
            } else {
                if(values[0] <= 40){
                    if(values[2] <= 17){
                        return SegmentType.NOISE;
                    } else {
                        return SegmentType.CLEAN;
                    }
                } else {
                    return SegmentType.CLEAN;
                }
            }
        } else {
            return SegmentType.NOISE;
        }
    }
}
