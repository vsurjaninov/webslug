package ru.vsurjaninov.webslug.filter;

import ru.vsurjaninov.webslug.model.Chunk;

public enum EmptyText implements IFilter {

    INSTANCE {
        @Override
        public boolean apply(Chunk chunk) {
            return chunk.getTokens().isEmpty();
        }
    }
}
