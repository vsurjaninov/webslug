package ru.vsurjaninov.webslug.filter;

import ru.vsurjaninov.webslug.model.Chunk;
import ru.vsurjaninov.webslug.model.Token;
import ru.vsurjaninov.webslug.model.TokenType;

public enum UnprocessedTag implements IFilter {
    INSTANCE{
        @Override
        public boolean apply(Chunk chunk) {
            for(Token token: chunk.getTokens()){
                if(token.getType() == TokenType.UNPROCESSED_TAG){
                    return true;
                }
            }
            return false;
        }
    }
}
