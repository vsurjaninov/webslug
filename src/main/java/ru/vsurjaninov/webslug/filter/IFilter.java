package ru.vsurjaninov.webslug.filter;

import ru.vsurjaninov.webslug.model.Chunk;

public interface IFilter {
    boolean apply(Chunk chunk);
}
