package ru.vsurjaninov.webslug.filter;

import ru.vsurjaninov.webslug.model.Chunk;
import ru.vsurjaninov.webslug.model.providers.ElementProvider;

public enum UnwantedTag implements IFilter {

    INSTANCE;

    private final static String[] UNWANTED_TAGS = {
            "head",
            "style",
            "script",
            "input",
            "textarea",
            "option",
            "select",
            "noframes",
            "img",
            "hr",
            "meta",
            "link",
            "noscript",
            "applet",
            "object",
            "embed",
            "iframe",
            "aside",
            "meta",
            "footer"
    };

    @Override
    public boolean apply(Chunk chunk) {
        for(final String tag: UNWANTED_TAGS){
            if(chunk.getElementProvider().nodeName().equals(tag)){
                return true;
            }
            ElementProvider element = chunk.getElementProvider().parent();
            while (element != null){
                if(element.nodeName().equals(tag)){
                    return true;
                }
                element = element.parent();
            }
        }

        return false;
    }
}
