# WebSlug

WebSlug is an Java project which includes algorithms and utilities for extracting main textual content from arbitrary web page.

## Demo
To watch how it works
```
git clone https://bitbucket.org/vsurjaninov/webslug.git
git clone https://bitbucket.org/vsurjaninov/wce-server.git
cd webslug
mvn install
cd ..
cd wce-server
mvn compile exec:java
```

## Command-Line Options
* -eu <url>  - extract from url link
* -ef <path> - extract from html file
* -ed <path> - extract from directory with html files

* -t - generate training-set (--htmldir <Path>, --golddir <Path>,  --outdir  <Path>)
* -v - compare results (--textdir <Path>, --golddir <Path>)
